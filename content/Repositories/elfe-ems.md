Title: EMS ELFE
Date: 2023-10-11 17:00
Tags: python, forecast, elfe
Slug: elfe-ems
Contributor: EPV
Project: ELFE


### Repository location

* [https://github.com/Energies-citoyennes-en-Pays-de-Vilaine/EnergyManagmentSystem](https://github.com/Energies-citoyennes-en-Pays-de-Vilaine/EnergyManagmentSystem)


### Description

Python program that makes the core functionnalities of the ELFE project.
 
#### Inputs
1° Past consumption data
2° Past energy balance (or power production)
3° User declarations of flexibilities

#### Outputs
 1° Energy balance forecast
 2° Optimized dispatch planning of the declared flexibilities.
 
