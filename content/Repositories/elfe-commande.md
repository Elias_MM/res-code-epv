Title: Commande ELFE
Date: 2023-10-11 17:00
Tags: python, control, elfe, mqtt
Slug: elfe-commande
Contributor: EPV
Project: ELFE


### Repository location

* [https://github.com/Energies-citoyennes-en-Pays-de-Vilaine/commande](https://github.com/Energies-citoyennes-en-Pays-de-Vilaine/commande)


### Description

Python program that uses a pre-built dispatch planning (produced by an EMS) to send orders to devices inside user homes and offices, through an MQTT Broker.
The program is also in charge of managing flexibility declarations and opt-outs, through a mqtt-connected screen on the user side.
 
#### Inputs
1° Dispatch planning of the declared flexibilities
2° Database of devices linked to each user's equipments


#### Outputs
 1° Power-on, power-off an keep-off orders
 2° Validated flexibility declarations (to be used by the EMS) 
 
