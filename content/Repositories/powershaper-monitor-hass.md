Title: Powershaper monitor HASS integration
Date: 2023-09-11 11:00
Tags: python, powershaper, homeassistant, hass
Slug: powershaper-monitor-hass
Contributor: CarbonCoop
Project: REScoopVPP

### Repository location

[https://gitlab.com/carboncoop/powershaper-monitor-hass](https://gitlab.com/carboncoop/powershaper-monitor-hass)

### Description
A homeassistant integration for pulling smart meter data from Carbon Co-op's Powershaper service and storing as a home assistant entity. This integration also has the option of pushing data to the COFYcloud.