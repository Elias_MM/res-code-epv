Title: Enercoop
Tags: France
Slug: Enercoop
Date: 2023-10-11 17:00

<img src="https://images.prismic.io/enercoop-corpo-production/3d769d2d-b1f1-4dbb-b254-9779b2b2e629_logo.svg?auto=compress" width="200" />


The cooperative Enercoop was created in 2005 to offer a citizen-led alternative when the energy market was liberalized. Through our network of 11 cooperatives spread all over the French territory, we empower citizens, communities, public entities and companies in the energy sector. The cooperatives act and campaign to make the energy transition local, citizen-led and inclusive. They produce and supply renewable energy and offer energy saving services. 18 years later, the energy supplier has 95 000 customers with more than 440 production sites contracted. We are part of more than 200 production projects. Our cooperatives network consists of more than 60 000 members.
Enercoop is highly committed to developing renewable energies, reducing the waste of resources, putting humans before profit, developing energy solidarity and supporting cooperatives as an alternative. These 5 commitments form the purpose of our cooperatives.



Visit [Enercoop's website](https://www.enercoop.fr/) for more information.

### Projects

- [REScoopVPP](/rescoopvpp)

### Repositories

- [enda](/enda)