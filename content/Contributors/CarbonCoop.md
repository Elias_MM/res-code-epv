Title: Carbon Coop
Tags: United Kingdom
Slug: carboncoop
Date: 2023-09-11 11:00

<img src="../images/carboncoop.png" width="200" />

Carbon Co-op is an energy services and advocacy co-operative that helps people and communities to make the radical reductions in home carbon emissions necessary to avoid runaway climate change. The organisation is based in Manchester, UK and was established in 2008. Carbon co-op work in energy systems and specialise in home energy management systems and related data collection and processing systems. Carbon Co-op have been involved in various projects in this area, including REScoopVPP which includes software and hardware development. Visit the [website](https://carbon.coop/) to find out more. 

### Projects

- [REScoopVPP](/rescoopvpp)