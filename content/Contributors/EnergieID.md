Title: EnergieID
Tags: Belgium
Slug: EnergieID
Date: 2023-09-11 11:00

<img src="../images/energieid.png" width="200" />

EnergieID is a Belgian cooperative that develops and maintains a energy management platform. The platform is used by energy cooperatives, local authorities and energy suppliers to offer energy services to their members, citizens and customers. The platform is also used by energy communities to manage their own energy assets.

Visit [EnergieID's website](https://energieid.be) for more information.

### Projects

- [REScoopVPP](/rescoopvpp)

### Repositories

- [cofybox-balena](/cofybox-balena)
- [cofybox-dce](/cofybox-dce)
- [cofybox-dce-azure-functions](/cofybox-dce-azure-functions)
- [cofybox.io](/cofybox.io)
- [d2lparser](/d2lparser)