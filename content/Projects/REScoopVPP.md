Title: RescoopVPP
Date: 2023-09-11 11:00
Tags: H2020
Slug: rescoopvpp
Contributors: EnergieID
    CarbonCoop

<img src="https://custom-images.strikinglycdn.com/res/hrscywv4p/image/upload/c_limit,fl_lossy,h_300,w_300,f_auto,q_100/1060699/589962_416511.png" width=200>

REScoopVPP is a Horizon2020-funded project that aims to set-up a community-driven virtual power plant that can actually provide flexibility services to the grid and contributes to a 100% share of renewable energy sources into the grid.
Visit [REScoopVPP's website](https://rescoopvpp.eu) for more information.

### Repositories

- [cofybox-balena](/cofybox-balena)
- [cofybox-dce](/cofybox-dce)
- [cofybox-dce-azure-functions](/cofybox-dce-azure-functions)
- [cofybox.io](/cofybox.io)
- [d2lparser](/d2lparser)
- [enda](/enda)